﻿using Festival_WebApp.Data;
using Festival_WebApp.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Data.Entity;

namespace Festival_WebApp.Controllers
{
    public class ArtistController : Controller
    {
        private readonly myDbContext db = new myDbContext(); // Replace YourDbContext with your actual DbContext name


        public ActionResult Index()
        {
            var artists = db.Artists.ToList();

            return View(artists);
        }


        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Artist artist)
        {
            if (ModelState.IsValid)
            {
                db.Artists.Add(artist);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            else
            {
                // If there are validation errors, display the errors
                foreach (var modelState in ModelState.Values)
                {
                    foreach (var error in modelState.Errors)
                    {
                        Console.WriteLine("Validation error: " + error.ErrorMessage);
                    }
                }
            }

            return View(artist);
        }

        // GET: Concert/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return BadRequest();
            }




            Artist artist = db.Artists.Find(id);
            if (artist == null)
            {
                return NotFound();
            }
            return View(artist);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Artist artist)
        {
            if (ModelState.IsValid)
            {
                db.Entry(artist).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            else
            {
                foreach (var modelState in ModelState.Values)
                {
                    foreach (var error in modelState.Errors)
                    {
                        Console.WriteLine("Validation error: " + error.ErrorMessage);
                    }
                }
            }
            return View(artist);
        }

        // GET: Concert/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return BadRequest();
            }
            Artist artist = db.Artists.Find(id);

            if (artist == null)
            {
                return NotFound();
            }


            return View(artist);
        }
        // GET: Concert/Details/5
        public ActionResult Details(int id)
        {
            var artist = db.Artists.FirstOrDefault(a => a.Id == id);
            if (artist == null)
            {
                return NotFound(); // Or handle not found scenario
            }

            return View(artist); // Pass the Concert to the Details view
        }
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Artist artist = db.Artists.Find(id);
            db.Artists.Remove(artist);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
