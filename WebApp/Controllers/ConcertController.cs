﻿
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;
using Festival_WebApp.Models;
using System.Data.Entity;
using Festival_WebApp.Data;
using System;

namespace Festival_WebApp.Controllers
{
    public class ConcertController : Controller
    {
        private readonly myDbContext db = new myDbContext(); // Replace YourDbContext with your actual DbContext name

        
        public ActionResult Index()
        {
            List<Concert> concerts = db.Concerts.Include(c => c.Artist).ToList();
            ViewBag.concerts = concerts;
                return View();
        }

   
        public ActionResult Create()
        {
            ViewBag.Artists = db.Artists.ToList(); // Retrieve artists for dropdown or selection

            return View();
        }

       [HttpPost]
       [ValidateAntiForgeryToken]
      public async Task<IActionResult> Create([Bind("Title, ArtistId, Date, Description")] Concert concert)
      {

            Console.WriteLine(concert.ArtistId);


            concert.Artist = db.Artists.Find(concert.ArtistId);
            
            if (ModelState.IsValid || !ModelState.IsValid && concert.ArtistId!=null)
          {
              // Fetch selected artist from database
              db.Concerts.Add(concert);
              await db.SaveChangesAsync();
              return RedirectToAction(nameof(Index));
          }
          
          else
            {
                // If there are validation errors, display the errors
                foreach (var modelState in ModelState.Values)
                {
                    foreach (var error in modelState.Errors)
                    {
                        Console.WriteLine("Validation error: " + error.ErrorMessage);
                    }
                }
            }
            ModelState.Remove("ArtistId");
            ViewBag.Artists = db.Artists.ToList();
            return View(concert);
        }
      /* [HttpPost]
       [ValidateAntiForgeryToken]
       public ActionResult Create(Concert concert )
       {
           if (ModelState.IsValid)
           {
               db.Concerts.Add(concert);
               db.SaveChanges();
               return RedirectToAction("Index");
           }
           else
           {
               // If there are validation errors, display the errors
               foreach (var modelState in ModelState.Values)
               {
                   foreach (var error in modelState.Errors)
                   {
                       Console.WriteLine("Validation error: " + error.ErrorMessage);
                   }
               }
           }

           return View(concert);
       }*/

        // GET: Concert/Edit/5
        public ActionResult Edit(int? id)

        {
            if (id == null)
            {
                return BadRequest();
            }




            Concert concert = db.Concerts.Find(id);
            if (concert == null)
            {
                return NotFound();
            }
            List<Artist> artists = db.Artists.ToList();

            ViewBag.Artists = artists;
            return View(concert);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Concert concert )
        {
            Console.WriteLine(concert.ArtistId);


            concert.Artist = db.Artists.Find(concert.ArtistId);

            if (ModelState.IsValid || !ModelState.IsValid && concert.ArtistId != null)
            {
                db.Entry(concert).State = (System.Data.Entity.EntityState)EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            else
            {
                // If there are validation errors, display the errors
                foreach (var modelState in ModelState.Values)
                {
                    foreach (var error in modelState.Errors)
                    {
                        Console.WriteLine("Validation error: " + error.ErrorMessage);
                    }
                }
            }
            ModelState.Remove("ArtistId");
            ViewBag.Artists = db.Artists.ToList();
            return View(concert);
        }

        // GET: Concert/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return BadRequest();
            }
            Concert concert = db.Concerts.Find(id);

            if (concert == null)
            {
                return NotFound();
            }


            return View(concert);
        }
        // GET: Concert/Details/5
        public ActionResult Details(int id)
        {
            var concert = db.Concerts.FirstOrDefault(a => a.Id == id);
            if (concert == null)
            {
                return NotFound(); // Or handle not found scenario
            }

            return View(concert); // Pass the Concert to the Details view
        }
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Concert concert = db.Concerts.Find(id);
            db.Concerts.Remove(concert);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
