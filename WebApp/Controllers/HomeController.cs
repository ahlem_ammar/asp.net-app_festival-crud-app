﻿using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;
using Festival_WebApp.Models;
using System.Data.Entity;
using Festival_WebApp.Data;
using System.Collections.Generic;

namespace Festival_WebApp.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly myDbContext db = new myDbContext();

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
            using (var dbContext = new myDbContext())
            {
                try
                {
                    dbContext.Database.Connection.Open();
                    var concerts = dbContext.Concerts.ToList();


                    var command = dbContext.Database.Connection.CreateCommand();
                    command.CommandText = "SELECT 1";
                    var result = command.ExecuteScalar();


                    if (result != null && result.ToString() == "1")
                    {
                        Console.WriteLine("Connection successful!");
                    }
                    else
                    {
                        Console.WriteLine("Connection failed!");
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine($"Connection error: {ex.Message}");
                }
            }
        }

        public IActionResult Index()
        {
           
            List<Concert> concerts = db.Concerts.Include(c => c.Artist).ToList();
            List<Artist> artists = db.Artists.ToList();

            ViewBag.Concerts = concerts;
            ViewBag.Artists = artists;
            return View();
        }

        public ActionResult Details(int id)
        {
            var concert = db.Concerts.FirstOrDefault(a => a.Id == id);
            if (concert == null)
            {
                return NotFound(); // Or handle not found scenario
            }

            return View(concert); // Pass the Concert to the Details view
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}