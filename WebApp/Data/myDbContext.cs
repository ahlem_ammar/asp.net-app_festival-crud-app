﻿using System.Collections.Generic;
using Festival_WebApp.Models;
using System.Data.Entity;

namespace Festival_WebApp.Data
{
    public class myDbContext : DbContext
    {

        public DbSet<Concert> Concerts { get; set; } // DbSet for the Concert entity
        public DbSet<Artist> Artists { get; set; }


        public myDbContext() : base(nameOrConnectionString: "Data Source=(localdb)\\ProjectModels;Initial Catalog=myDb;Integrated Security=True;Connect Timeout=30;Encrypt=False;")
        {
        }

        // Add other DbSets for additional entities if needed

        // Optionally, you can override the OnModelCreating method for further configurations
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {

            // Configure model here (e.g., relationships, constraints)
            base.OnModelCreating(modelBuilder);
        }
    }
}


