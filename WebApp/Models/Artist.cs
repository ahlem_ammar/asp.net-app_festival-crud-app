﻿namespace Festival_WebApp.Models
{
    using System;
    using System.ComponentModel.DataAnnotations;

    public class Artist
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [MaxLength(100)]
        public string Name { get; set; }

        [Required]
        
        public string Categorie { get; set; }

     
    }
}
